# Create a Trivia Game using Vue.js
## Assignment # 5, Experis Acadamy

This assignmnet is to build an online triva game as a Single Page Application using the Vue.js framework.
Assignment details are found in the Assignment 5_Vue_Trivia Game.pdf 

The trivia game can be played here: [trivia-game](https://protected-forest-96328.herokuapp.com/#/)

## Project setup
```
npm install
```


