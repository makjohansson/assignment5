import Start from '../components/Start/StartPage'
import Question from '../components/Question/QuestionPage'
import Result from '../components/Result/ResultPage'
import VueRouter from 'vue-router' 
import store from '../store/index'

/**
 * Vue-route and navguard setup
 */

const routes = [
    {
        path: '/', 
        name: "start", 
        component: Start
    },
    {
        path: '/question', 
        name: "question", 
        component: Question,
        beforeEnter: (to, from, next) => {
            if(!store.getters.gameStarted) {
                next("/");
            } else {
                next();
            }
        }
    },
    {
        path: '/result', 
        name: "result", 
        component: Result,
        beforeEnter: (to, from, next) => {
            if(!store.getters.endOfGame) {
                next("/");
            } else {
                next();
            }
        }
    }
]

const router = new VueRouter({routes});

export default router;