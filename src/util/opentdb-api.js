const CATEGORY_API_URL = "https://opentdb.com/api_category.php";
const CATEGORY_COUNT_API_URL = "https://opentdb.com/api_count.php?category=";
const SESSION_TOKEN_API_URL = "https://opentdb.com/api_token.php?command=request";
const RESET_SESSION_TOKEN_API_URL = "https://opentdb.com/api_token.php?command=reset&token=";

// get questions for the quiz
export const get_questions = (apiUrl) => {
    return fetch(apiUrl)
    .then(response => response.json())
    .then(response => response)
}

// gets all categories
export const get_categories = () => {
    return fetch(CATEGORY_API_URL)
    .then(response => response.json())
    .then(response => response.trivia_categories)
}

// get the max number of questions for the chosen category
export const get_number_of_questions = (id) => {
    return fetch(`${CATEGORY_COUNT_API_URL}${id}`)
    .then(response => response.json())
    .then(response => response.category_question_count)
} 

// creates a session token
export const get_session_token = () => {
    return fetch(SESSION_TOKEN_API_URL)
    .then(response => response.json())
    .then(response => response.token)
}

// resets the session token
export const reset_session_token = (token) => {
    return fetch(RESET_SESSION_TOKEN_API_URL + token)
    .then(response => response.json())
    .then(response => response)
}