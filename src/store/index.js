import Vue from 'vue';
import VueX from 'vuex';
import {get_questions, get_session_token, reset_session_token} from '../util/opentdb-api';
Vue.use(VueX);

/**
 * Vue store to handle using vuex for state managment.
 */
export default new VueX.Store({
    state: {
        questions: [],
        index: 0,
        totalScore: 0,
        correctAnswers: [],
        userAnswers: [],
        answerdQuestions: [],
        apiUrl: '',
        sessionToken: '',
        gameStarted: false,
        endOfGame: false
    },
    mutations: {
        setQuestions: (state, payload) => {
            state.questions = payload;
        },
        incrementIndex: (state) => {
            state.index++;
        },
        setCorrectAnswers: (state, payload) => {
            state.correctAnswers.push(payload);
        },
        setUserAnswers: (state, payload) => {
            state.userAnswers = payload;
        },
        addToAnswerdQuestions: (state, payload) => {
            state.answerdQuestions.push(payload);
        },
        incrementScore: (state) => {
            state.totalScore++;
        },
        setApiUrl: (state, payload) => {
            state.apiUrl = payload;
        },
        setSessionToken: (state, payload) => {
            state.sessionToken = payload;
        },
        resetGameValues: (state) => {
            state.totalScore = 0;
            state.index = 0;
            state.correctAnswers = [];
            state.userAnswers = [];
            state.answerdQuestions = [];
            state.questions = [];
        },
        resetHardValues: (state) => {
            state.totalScore = 0;
            state.index = 0;
            state.correctAnswers = [];
            state.userAnswers = [];
            state.answerdQuestions = [];
            state.questions = [];
            state.gameStarted = false;
            state.endOfGame = false;
            state.sessionToken = '';
        },
        setGameStarted: (state, payload)  => {
            state.gameStarted = payload;
        },
        setEndOfGame: (state, payload)  => {
            state.endOfGame = payload;
        }
    }, 
    actions : {
        /**
         * Api call to opendb to get questions with session token.
         * If response code == 4, session token will reset.
         * @param {*} param0 
         */
        async loadQuestions({ state, commit }) {
            const token = state.sessionToken === '' ? '' : `&token=${state.sessionToken}`;
            let apiResponse = await get_questions(state.apiUrl + token);
            if (apiResponse.response_code == 4) {
                await reset_session_token(state.sessionToken);
                apiResponse = await get_questions(state.apiUrl + token);
            } 
            commit('setQuestions', apiResponse.results);
        },
        /**
         * Question index incremented by one
         * @param {*} param0 
         */
        incrementQuestionIndex({ commit }) {
            commit('incrementIndex');
        },
        /**
         * Save a answerd question. Saves the correct answer, answerd answer, and the question.
         * To be displayed at the result page.
         * @param {*} selectedAnswer the answer selected by the user
         */
        saveAnswerdQuestion({ state , commit }, selectedAnswer) {
            const currentQuestion = state.questions[state.index];
            const answerdQuestion = {
                correctAnswer: currentQuestion.correct_answer,
                selectedAnswer: selectedAnswer,
                question: currentQuestion.question
            }
            commit('addToAnswerdQuestions', answerdQuestion);
        },
        /**
         * Increment the score by one
         */
        addToScore ({commit}) {
            commit('incrementScore');
        },
        /**
         * Save api url. Use to get correct questions after what a user selected at the start page.
         * @param {*} apiUrl url for a specific api call.
         */
        saveApiUrl({commit}, apiUrl) {
            commit('setApiUrl', apiUrl );
        },
        /**
         * Get a session token from opendb and save it to the store
         */
        async saveSessionToken({ commit }) {
            const sessionToken = await get_session_token();
            commit('setSessionToken', sessionToken);
        },
        /**
         * Reset to play one more quiz without returning to the start page.
         */
        reset({commit}) {
            commit('resetGameValues');
        },
        /**
         * Reset and returning to the satart page.
         */
        resetHard({commit}) {
            commit('resetHardValues')
        },
        /**
         * True if user selected to start quiz
         * @param {*} status if true, route to the question page is allowed
         */
        updateGameStarted({commit}, status) {
            commit('setGameStarted', status);
        },
        /**
         * True if user finished the quiz and route to the result page is allowed
         * @param {*} status if true route to result page
         */
        updateEndOfGame({commit}, status) {
            commit('setEndOfGame', status);
        }
    },
    getters: {
        gameStarted: state => state.gameStarted,
        endOfGame: state => state.endOfGame,
        numOfQuestions: state => state.questions.length
    },
})